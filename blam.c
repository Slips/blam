#include <mysql/mysql.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <json-c/json.h>
#include "./colors/colors.h"

#define _POSIX_C_SOURCE 200809L


/*CONFIG*/

#define dbLogin "blam" /*MySQL Username*/
#define dbPass "password123" /*MySQL Password*/
#define dbName "blam" /*Database Name*/
#define dbPort "localhost" /*Connection Port. If you don't know, don't touch*/
#define argv_parse_len 100 /*For debugging purposes only.*/

#define defaultPath "~/gitstuffs/aur/" /*Default path for packages. set to NO PATH to ignore.*/
/*CODE*/
typedef json_object JSON;
int
updatePath() {
        MYSQL *con;
        size_t len = 128;
        char pkgname[len];
        char newpath[len];
        char sql_statement[2048];
        con = mysql_init(NULL);
        for(unsigned long i = 0; i < len; i++) {
          pkgname[i] = '\0';
        }
        for(unsigned long i = 0; i < len; i++) {
          newpath[i] = '\0';
        }
        if( mysql_real_connect(con, dbPort, dbLogin, dbPass, dbName, 3306, NULL, 0) == NULL)
        {
          printf(C_RED "FATAL: Authentication Failure! MySQL Return Message: \n");
          printf("%s\n" C_RESET, mysql_error(con));
          return 1;
        }
        fputs("\n\nWhich package would you like to update the path to?: ", stdout);
        fgets(pkgname, len, stdin);
        if(pkgname[0] == 10){
          fputs(C_RED "FATAL: Input is empty\n" C_RESET, stdout);
          return 1;
        } else {
          strtok(pkgname, "\n");
        }
        fputs("\n\nWhat is the new path?: ", stdout);
        fgets(newpath, len, stdin);
        if(newpath[0] == 10){
          fputs(C_RED "FATAL: Input is empty\n" C_RESET, stdout);
          return 1;
        } else {
          strtok(newpath, "\n");
        }

        sprintf(sql_statement, "UPDATE packages SET path=\"%s\" WHERE name=\"%s\"", newpath, pkgname);
        if(mysql_query(con, sql_statement) !=0)
        {
          printf(C_RED "FATAL: Query Failure! MySQL Return Message:\n");
          printf("%s\n" C_RESET, mysql_error(con));
          return 1;
        }
  return 0;
}
int
gitPkg(){
        MYSQL *con;
        size_t len = 128;
        char pkgname[len];
        char newpath[len];
        char sql_statement[2048];
        con = mysql_init(NULL);
        for(unsigned long i = 0; i < len; i++) {
          pkgname[i] = '\0';
        }
        for(unsigned long i = 0; i < len; i++) {
          newpath[i] = '\0';
        }
        if( mysql_real_connect(con, dbPort, dbLogin, dbPass, dbName, 3306, NULL, 0) == NULL)
        {
          printf(C_RED "FATAL: Authentication Failure! MySQL Return Message: \n");
          printf("%s\n" C_RESET, mysql_error(con));
          return 1;
        }
        fputs("\nWhich package would you like to mark as a -git package?: ", stdout);
        fgets(pkgname, len, stdin);
        if(pkgname[0] == 10){
          fputs(C_RED "FATAL: Input is empty\n" C_RESET, stdout);
          return 1;
        } else {
          strtok(pkgname, "\n");

        }
        sprintf(sql_statement, "UPDATE packages SET isGit = 1 WHERE name=\"%s\"", pkgname);
        if(mysql_query(con, sql_statement) !=0)
        {
          printf(C_RED "FATAL: Query Failure! MySQL Return Message:\n");
          printf("%s\n" C_RESET, mysql_error(con));
          return 1;
        }
        return 0;

}
int
addPkg(char *input, char *arg2path){
        pid_t finished;
        pid_t status;
        size_t len = 128;
        char path[len];
        MYSQL *con;
        con = mysql_init(NULL);
        if( mysql_real_connect(con, dbPort, dbLogin, dbPass, dbName, 3306, NULL, 0) == NULL)
        {
          printf(C_RED "FATAL: Authentication Failure! MySQL Return Message: \n");
          printf("%s\n" C_RESET, mysql_error(con));
          return 1;
        }
        char baseURL[argv_parse_len+52] = {"https://aur.archlinux.org/rpc/?v=5&type=info&arg="};
        char *fullURL[] = {"curl", "-s", strncat(baseURL, input, argv_parse_len), "-o", "/tmp/blam-pkginfo.json", (char *)0};
        pid_t forkPID = fork();
        switch (forkPID) {
        case -1: /*pow is infertile for whatever reason*/
            return 1;
            break;;
        case 0: /*pow has successfully conceived*/
            errno = 0;
            execvp("curl", fullURL);
            printf("%i: %s\n",errno, strerror(errno));
            return -errno;
            break;;
        default:
            finished = wait(&status);

            if(finished != forkPID){
                fputs("FATAL: got PID of unknown process (Expected PID of Child)\n", stderr);
                return 1;
            }
            break;;
            }
        /*Slowly but surely get the version number from the latest JSON*/
        json_object *pkginfo = json_object_from_file("/tmp/blam-pkginfo.json");
        if(!pkginfo) {
          fputs(C_RED "FATAL: Error in JSON parsing." C_RESET, stderr);
          return 1;
        }
        json_object *results = json_object_object_get(pkginfo, "results");
        if(!results) {
          fputs(C_RED"FATAL: No Package Found with that name." C_RESET, stderr);
          return 1;
        }
        json_object *arrayItem = json_object_array_get_idx(results, 0);
        json_object *name = json_object_object_get(arrayItem, "Name");
        json_object *desc = json_object_object_get(arrayItem, "Description");
        json_object *version = json_object_object_get(arrayItem, "Version");
        const char *Sname = json_object_get_string(name);
        const char *Sdesc = json_object_get_string(desc);
        const char *Sversion = json_object_get_string(version);
        if(!pkginfo) {
          fputs(C_RED "FATAL: Something went wrong." C_RESET, stdout);
          return 1;
        }
        char sql_statement[2048];
        if(!strcmp(arg2path, "NO PATH")) {
          printf("Where was this installed? ");
          fgets(path, len, stdin);
          if(path[0] == 10) {
            fputs(C_RED "FATAL: No Path Provided", stdout);
            return 1;
          } else {
            strtok(path, "\n");
          }
        sprintf(sql_statement, "INSERT INTO packages(name, description, version, path) VALUES (\"%s\", \"%s\", \"%s\", \"%s\");", Sname, Sdesc, Sversion, path);
        } else {
          char presetPath[512] = {0};
          strcat(presetPath, arg2path);
          strcat(presetPath, Sname);
          sprintf(sql_statement, "INSERT INTO packages(name, description, version, path) VALUES (\"%s\", \"%s\", \"%s\", \"%s\");", Sname, Sdesc, Sversion, presetPath);
        }

        if(mysql_query(con, sql_statement) !=0)
        {
          printf(C_RED "FATAL: Query Failure! MySQL Return Message:\n");
          printf("%s\n" C_RESET, mysql_error(con));
          return 1;
        }
        printf("Successfully added package " C_BOLD "%s.\n\n" C_RESET, json_object_get_string(name));
        return 0;
}
int
verComparison(char *input[]){
        pid_t finished;
        pid_t status;
        char baseURL[argv_parse_len+52] = {"https://aur.archlinux.org/rpc/?v=5&type=info&arg="};
        char *fullURL[] = {"curl", "-s", strncat(baseURL, input[1], argv_parse_len), "-o", "/tmp/blam-pkginfo.json", (char *)0};
        pid_t forkPID = fork();
        switch (forkPID) {
        case -1: /*pow is infertile for whatever reason*/
            return 1;
            break;;
        case 0: /*pow has successfully conceived*/
            errno = 0;
            execvp("curl", fullURL);
            printf("%i: %s\n",errno, strerror(errno));
            return -errno;
            break;;
        default:
            finished = wait(&status);

            if(finished != forkPID){
                fputs("FATAL: got PID of unknown process (Expected PID of Child)\n", stderr);
                return 1;
            }
            break;;
            }
        /*Slowly but surely get the version number from the latest JSON*/
        json_object *pkginfo = json_object_from_file("/tmp/blam-pkginfo.json");
        json_object *results = json_object_object_get(pkginfo, "results");
        json_object *arrayItem = json_object_array_get_idx(results, 0);
        json_object *version = json_object_object_get(arrayItem, "Version");
        if(!pkginfo) {
          fputs(C_RED "FATAL: Something went wrong." C_RESET, stdout);
          return 1;
        }
        char Sversion[64];
        char Srow[64];
        for(int i = 0; i < 64; i++) {
          Srow[i] = '\0';
        }
        for(int i = 0; i < 64; i++) {
          Sversion[i] = '\0';
        }
        strcat(Sversion, json_object_get_string(version));
        strcat(Srow, input[3]);

        if(strcmp(Sversion,Srow) == 0){
          printf(C_BOLD "%s up to date." C_RESET " (%s)\n\n", input[1], Srow);
          json_object_put(pkginfo);
          return 0;
        } else {
          printf(C_BOLD"%s" C_RED " OUT OF DATE." C_RESET C_BOLD "\n\nLocal Version:"C_RESET" %s\n"C_BOLD"Latest Version:"C_RESET" %s\n" C_BOLD "Path: " C_RESET "%s\n\n", input[1], Srow, Sversion, input[4]);
          json_object_put(pkginfo);
          return 1;
        }
}
int
updateCheck(){
  MYSQL *con;
  char breakDown[2048];
  char breakDown2[2048];
  char buf[1024] = {0};
  char buf2[1024] = {0};
  con = mysql_init(NULL);
  if( mysql_real_connect(con, dbPort, dbLogin, dbPass, dbName, 3306, NULL, 0) == NULL)
  {
    printf(C_RED "FATAL: Authentication Failure! MySQL Return Message: \n");
    printf("%s\n" C_RESET, mysql_error(con));
    return 1;
  }
  char *query = "SELECT * FROM packages;";
    if (mysql_query(con, query) != 0)
  {
    fprintf(stderr, C_RED "FATAL: Query Failure! MySQL Return Message: \n%s\n", mysql_error(con));
    return 1;
  } else {
    MYSQL_RES *query_results = mysql_store_result(con);
    if (query_results) { /*make sure there *are* results..*/
      MYSQL_ROW row;

      while((row = mysql_fetch_row(query_results)) !=0)
      {
        /*If the 5th row (isGit) == NULL, run standard vercomp*/
        if(row[5] == NULL) {
          if(verComparison(row)) {
            sprintf(buf, C_BOLD "NAME"C_RESET": %s | "C_BOLD"PATH"C_RESET" %s\n", row[1], row[4]);
            strcat(breakDown, buf);
          }
        } else {
          printf(C_ITALIC "\"%s\" marked as VCS, skipping. Path: %s\n\n" C_RESET,row[1], row[4]);
          sprintf(buf2, C_BOLD "NAME"C_RESET": %s | "C_BOLD"PATH"C_RESET" %s\n", row[1], row[4]);
          strcat(breakDown2, buf2);
        }
        if((row = mysql_fetch_row(query_results)) !=0) {
            if(row[5] == NULL) {
              if(verComparison(row)) {
                sprintf(buf, C_BOLD "NAME"C_RESET": %s | "C_BOLD"PATH"C_RESET" %s\n", row[1], row[4]);
                strcat(breakDown, buf);
              }
            } else {
              printf(C_ITALIC "\"%s\" marked as VCS, skipping. Path: %s\n\n" C_RESET,row[1], row[4]);
              sprintf(buf2, C_BOLD "NAME"C_RESET": %s | "C_BOLD"PATH"C_RESET" %s\n", row[1], row[4]);
              strcat(breakDown2, buf2);
            }
          while((row = mysql_fetch_row(query_results)) != 0) {
            if(row[5] == NULL) {
              if(verComparison(row)) {
                sprintf(buf, C_BOLD "NAME"C_RESET": %s | "C_BOLD"PATH"C_RESET" %s\n", row[1], row[4]);
                strcat(breakDown, buf);
              }
            } else {
              printf(C_ITALIC "\"%s\" marked as VCS, skipping. Path: %s\n\n" C_RESET,row[1], row[4]);
              sprintf(buf2, C_BOLD "NAME"C_RESET": %s | "C_BOLD"PATH"C_RESET" %s\n", row[1], row[4]);
              strcat(breakDown2, buf2);
            }
          }
        }
      }

    }
      /* Free results when done */
      mysql_free_result(query_results);
      printf(C_BOLD"Out Of Date Package Overview:"C_RESET"\n\n%s", breakDown);
      printf(C_BOLD"Git Packages:"C_RESET"\n\n%s", breakDown2);
  }
  return 0;
}
int
listPackages(){
  MYSQL *con;
  con = mysql_init(NULL);
  if( mysql_real_connect(con, dbPort, dbLogin, dbPass, dbName, 3306, NULL, 0) == NULL)
  {
    printf(C_RED "FATAL: Authentication Failure! MySQL Return Message: \n");
    printf("%s\n" C_RESET, mysql_error(con));
    return 1;
  }
  char *query = "SELECT * FROM packages;";
    if (mysql_query(con, query) != 0)
  {
    fprintf(stderr, C_RED "FATAL: Query Failure! MySQl Return Message: %s\n" C_RESET, mysql_error(con));
    return 1;
  } else {
    MYSQL_RES *query_results = mysql_store_result(con);
    if (query_results) { /*make sure there *are* results..*/
      MYSQL_ROW row;

      while((row = mysql_fetch_row(query_results)) !=0)
      {
        printf("(%s) %s "C_BOLD"|"C_RESET" %s "C_BOLD"|"C_RESET" %s "C_BOLD"|"C_RESET" %s \n", row[0], row[1], row[2], row[3], row[4]);
        if((row = mysql_fetch_row(query_results)) !=0) {

        for(int i = 0; i < 3; i++);

        printf("(%s) %s "C_BOLD"|"C_RESET" %s "C_BOLD"|"C_RESET" %s"C_BOLD"|"C_RESET" %s \n", row[0], row[1], row[2], row[3], row[4]);
        }
      }

      /* Free results when done */
    }
      mysql_free_result(query_results);
  }
  return 0;
}
int
updateDB(){
  MYSQL *con;
  size_t len = 128;
  char pkgname[len];
  char newver[len];
  char sql_statement[2048];
  con = mysql_init(NULL);
  for(unsigned long i = 0; i < len; i++) {
    pkgname[i] = '\0';
  }
  for(unsigned long i = 0; i < len; i++) {
    newver[i] = '\0';
  }
  if( mysql_real_connect(con, dbPort, dbLogin, dbPass, dbName, 3306, NULL, 0) == NULL)
  {
    printf(C_RED"FATAL: Authentication Failure! MySQL Return Message: \n");
    printf("%s\n"C_RESET, mysql_error(con));
    return 1;
  }
  fputs(C_BOLD"\n\nWhich package would you like to update the version of? (Name): "C_RESET, stdout);
  fgets(pkgname, len, stdin);
  if(pkgname[0] == 10){
    fputs(C_RED"FATAL: Input is empty\n"C_RESET, stdout);
    return 1;
  } else {
    strtok(pkgname, "\n");

  }
  printf(C_BOLD"\n\nOkay, what version of %s did you update to? "C_RESET, pkgname);
  fgets(newver, len, stdin);
  if(newver[0] == 10) {
    fputs(C_RED"FATAL: Input is empty\n"C_RESET, stdout);
    return 1;
  } else {
    strtok(newver, "\n");
  }
  sprintf(sql_statement, "UPDATE packages SET version = \"%s\" WHERE name=\"%s\";", newver, pkgname);
  if(mysql_query(con, sql_statement) !=0)
  {
    printf(C_RED"FATAL: Query Failure! MySQL Return Message:\n");
    printf("%s\n"C_RESET, mysql_error(con));
    return 1;
  }
  printf(C_RESET"Successfully changed package "C_BOLD"%s"C_RESET" to version "C_BOLD"%s\n"C_RESET, pkgname, newver);
  return 0;
}
int
removeItem(){
  MYSQL *con;
  con = mysql_init(NULL);
  size_t len = 128;
  char pkgname[len];
  char buf[10];
  char sql_statement[2048];

  if( mysql_real_connect(con, dbPort, dbLogin, dbPass, dbName, 3306, NULL, 0) == NULL)
  {
    printf(C_RED"FATAL: Authentication Failure! MySQL Return Message: \n");
    printf("%s\n"C_RESET, mysql_error(con));
    return 1;
  }
  fputs(C_BOLD"Which package would you like to remove from the database? "C_RESET, stdout);
  fgets(pkgname, len, stdin);
  if(pkgname[0] == 10) {
  fputs(C_RED"FATAL: Input is empty\n"C_RESET, stdout);
    return 1;
  } else {
    strtok(pkgname, "\n");
  }
  printf(C_BOLD C_ITALIC "Are you sure you want to delete %s? "C_RESET, pkgname);
  fgets(buf, 10, stdin);
  if(buf[0] == 89 || buf[0] == 121){
          sprintf(sql_statement, "DELETE FROM packages WHERE name = \"%s\"", pkgname);
          if(mysql_query(con, sql_statement) !=0)
          {
            printf(C_RED"FATAL: Query Failure! MySQL Return Message:\n");
            printf("%s\n"C_RESET, mysql_error(con));
            return 1;
          }
  printf(C_BOLD "%s"C_RESET" removed from database.", pkgname);
  } else {
    fputs(C_ITALIC"Okay, cancelling.", stdout);
  }



  


  mysql_close(con);
  return 0;
}
int
manualParse(){
  /*This function sucks ass, and will be deprecated in like 3 commits.
   *I hope.
   */
  MYSQL *con;
  con = mysql_init(NULL);
  size_t len = 128;
  char pkgname[len];
  char desc[len];
  char ver[len];
  char sql_statement[2048];

  if( mysql_real_connect(con, dbPort, dbLogin, dbPass, dbName, 3306, NULL, 0) == NULL)
  {
    printf(C_RED"FATAL: Authentication Failure! MySQL Return Message: \n");
    printf("%s\n"C_RESET, mysql_error(con));
    exit(1);
  }
  fputs(C_BOLD"Package Name: "C_RESET, stdout);
  fgets(pkgname, len, stdin);
  if(pkgname[0] == 10){
    fputs("FATAL: Input is empty\n", stdout);
    exit(1);
  }
  fputs(C_BOLD"Package Description (Optional): "C_RESET, stdout);
  fgets(desc, len, stdin);
  if(desc[0] == 10){
    sprintf(desc, "(No Description)");
  } else {
    strtok(desc, "\n");
  }
  fputs(C_BOLD"Package Version: "C_RESET, stdout);
  fgets(ver, len, stdin);
  if(ver[0] == 10){
    fputs("FATAL: Input is empty\n", stdout);
    return 1;
  }
  strtok(ver, "\n");
  strtok(pkgname, "\n");
  // Construct the SQL statement
  sprintf(sql_statement, "INSERT INTO packages(name, description, version) VALUES (\"%s\", \"%s\", \"%s\");", pkgname, desc, ver);
  if(mysql_query(con, sql_statement) !=0)
  {
    printf(C_RED"FATAL: Query Failure! MySQL Return Message:\n");
    printf("%s\n"C_RESET, mysql_error(con));
    exit(1);
  }


  mysql_close(con);
  exit(0);
}
void
helpMsg(){
  fputs("Blam - A simple AUR package databse and update checker.\n", stdout);
  fputs("-------------------------------------------------------\n", stdout);
  fputs("h                          |Print this message.\n", stdout);
  fputs("a <keyword>                |Add a package to the DB.\n", stdout);
  fputs("m                          |Manually add a package.\n",stdout);
  fputs("u                          |Check for package updates.\n",stdout);
  fputs("r                          |Remove a package from the DB.\n",stdout);
  fputs("l                          |List all packages.\n",stdout);
  fputs("v                          |Update a packages' version.\n", stdout);
  fputs("g                          |Flag a package in the DB as git.\n", stdout);
  fputs("p                          |Set a package's path.\n", stdout);
  fputs("-------------------------------------------------------\n", stdout);
}
int
main(int argc, char *argv[]) {
  MYSQL *con;
  con = mysql_init(NULL);
  char *sql_statement;
  char in_str[argv_parse_len] = {0};
  char in_str2[argv_parse_len] = {0};
  int newInstall = 0;
  /*Q: Would this be better as an explicit function you can run?\
   *A: No.
  */
  if( mysql_real_connect(con, dbPort, dbLogin, dbPass, dbName, 3306, NULL, 0) == NULL)
  {
    printf("FATAL: Authentication Failure! MySQL Return Message: \n");
    printf("%s\n", mysql_error(con));
    return 1;
  }
  sql_statement = "SELECT * FROM packages;";
  if(mysql_query(con, sql_statement) !=0) {
    fputs(C_ITALIC"It seems that the blam SQL tables do not exist. Initializing...\n\n"C_RESET, stdout);
    sql_statement = "CREATE TABLE packages(id int UNSIGNED NOT NULL AUTO_INCREMENT, name VARCHAR(256) NOT NULL, description VARCHAR(256), version VARCHAR(256) NOT NULL, path VARCHAR(256), isGit BIT, commitHash VARCHAR(256), PRIMARY KEY (id) );";
    if(mysql_query(con, sql_statement) !=0) {
      printf(C_RED"FATAL: Authentication Failure! MySQL Return Message: \n");
      printf("%s\n"C_RESET, mysql_error(con));
      return 1;
    }
    newInstall = 1;
  }
  mysql_close(con);
  if(argc == 1) {
      if(newInstall) {
        fputs(C_BOLD"Thank you for installing blam!\n\n"C_RESET, stdout);
        fputs("To add a a package, run "C_BOLD"blam a"C_RESET"\nTo check for updates, run "C_BOLD"blam u"C_RESET, stdout);
        fputs("\nTo mark a package as git (which will exempt it from package checking), run "C_BOLD"blam g"C_RESET, stdout);
        fputs("\n\nTo migrate from a new package manager, run the following:\n"C_BOLD"pacman -Qqm | xargs -n1 blam a\n"C_RESET, stdout);
        fputs("\n\n"C_ITALIC"This message will not show again. To see a more in depth help prompt run "C_RESET C_BOLD"blam h"C_RESET", or read the man pages for this software.\n", stdout);
        return 0;
      } else {
        helpMsg();
      }
    return 1;
  }
  switch (argv[1][0]) {
    case 'h':
      helpMsg();
      return 0;
    case 'r':
      in_str[strlen(in_str)-1] = '\0';
      removeItem();
      break;
    case 'm':
      manualParse();
      break;
    case 'l':
      fputs(C_ITALIC"All currently installed packages:\n"C_RESET, stdout);
      listPackages();
      break;
    case 'u':
      fputs(C_ITALIC"Checking for updates...\n\n"C_RESET, stdout);
      updateCheck();
      break;
    case 'g':
      gitPkg();
      break;
    case 'v':
      fputs(C_ITALIC"Updating Version:"C_RESET, stdout);
      updateDB();
      break;
    case 'a':
        strncat(in_str, argv[2], argv_parse_len-1);
        if(argc >= 4) {
          addPkg(in_str, in_str2);
        } else {
          addPkg(in_str, defaultPath);
        }
      break;
    case 'p':
      updatePath();
    default:
      return 0;
  }

}
