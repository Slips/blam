#ifndef COLORS_H
#define COLORS_H

#define ESC "\033["

#define C_RED        ESC"31m"
#define C_GREEN      ESC"32m"
#define C_BLUE       ESC"34m"

#define C_CYAN       ESC"36m"
#define C_PINK       ESC"33m"
#define C_PURPLE     ESC"35m"
#define C_WHITE      ESC"37m"
#define C_BLACK      ESC"30m"

#define C_BLINKING   ESC"5m"
#define C_BOLD       ESC"1m"
#define C_DIM        ESC"2m"
#define C_ITALIC     ESC"3m"
#define C_RESET      ESC"0m"
#define C_REVERSE    ESC"7m"
#define C_UNDERLINE  ESC"4m"

#define C_CLEAR      ESC"2J"  ESC"3J"

// Check if colors are enabled in stdout and stderr
#define C_ENABLED ( (C_NO_COLOR_DISABLED && C_TTY_OUT) || C_COLOR_ALWAYS )
#define C_ERR_ENABLED ( (C_NO_COLOR_DISABLED && C_TTY_ERR) || C_COLOR_ALWAYS )

extern int C_NO_COLOR_DISABLED;
extern int C_COLOR_ALWAYS;
extern int C_TTY_OUT;
extern int C_TTY_ERR;

void C_init();

#endif // COLORS_H header guard
