# Blam - A simple AUR package database and update checker.

## Installation:
Installation is somewhat more complicated than with `pow`. First, create an SQL user. This can be done by running `mysql -u root -p` as root, then pasting the following command into the SQL prompt:
```
CREATE USER 'blam'@'localhost'
  IDENTIFIED BY '[PasswordHere]';
GRANT ALL
  ON blam.*
  TO 'blam'@'localhost'
  WITH GRANT OPTION;
```
Take note of the password you use. While you're still in this prompt, create the blam SQL database with the following command:
```
CREATE DATABASE blam;
```
Now, add the username, password, database name (if you used one that wasn't "blam"), and AUR installation path (i.e where you clone AUR packagebuilds to) to the config section in blam.c.
Finally, run `make install` and then `blam`. You will receive a message that says the blam SQL tables do not exist, and they will be initialized for you.

## Compilation:
run `make`, or `cc blam.c -o blam (backtick)mysql_config --cflags --libs(backtick) -std=c99`

## Limitations:
blam cannot currently handle update checking for VCS packages (i.e -git). Because of how blam is written, the version will not be checked. Ignore this. Optionally, you can set a package as git with the `blam g` command which will exempt it from version checking.

## Migration:
Migrating from another package manager (i.e yay or paru) or from standard makepkg is simple. ALl you have to do is run the following command:
```
pacman -Qqm | xargs -n1 blam a
```
This will provide a list of every package not in a sync db (i.e any package installed from the AUR) and then add it to the blam package database.

Note that this will set their paths to the defaultPath variable set in the source code, rather then the path to the AUR repositories as cloned by your previous package manager. It is suggested that you either move the contents of the package manager clone directory (i.e ~/.cache/paru/clone) to your listed default path or reclone them.
