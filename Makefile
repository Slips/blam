##
# Zap: A lightning fast AUR Searcher written in C
#
# @file
# @version 0.1
.DEFAULT_GOAL := build
CFLAGS=-std=c99 -Wall -Wpedantic
OPT=-O3
LDFLAGS=-ljson-c `mysql_config --cflags --libs`
DESTDIR=/usr
CC=tcc

build:
	$(CC) blam.c -o blam $(OPT) $(CFLAGS) $(LDFLAGS)

man:
	scdoc < blam.1.scd > blam.1

install: build man
	cp -f blam $(DESTDIR)/bin/
	cp -f blam.1 $(DESTDIR)/share/man/man1/; true
# end
debug:
	gcc blam.c -o blam -g $(CFLAGS) $(LDFLAGS)
